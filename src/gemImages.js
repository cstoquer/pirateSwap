import ui.resource.Image as Image;

exports = [
	null,
	new Image({url: 'resources/images/gem_diamond.png'}),
	new Image({url: 'resources/images/gem_emerald.png'}),
	new Image({url: 'resources/images/gem_lazuli.png'}),
	new Image({url: 'resources/images/gem_opal.png'}),
	new Image({url: 'resources/images/gem_gold.png'}),
	new Image({url: 'resources/images/gem_ruby.png'}),
	new Image({url: 'resources/images/gem_bottle.png'}),
	new Image({url: 'resources/images/gem_chest.png'}),
	new Image({url: 'resources/images/gem_bomb.png'})
];
